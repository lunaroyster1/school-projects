print "Choose conversion:"
print "1 = ASCII Code to Character"
print "2 = Character to ASCII Code"
choice = int(raw_input("Enter 1 or 2: "))

if choice == 1:
    ascii = int(raw_input("Enter ASCII code [ex: 103, 94]: "))
    print "The character for ASCII code", ascii, "is", chr(ascii)
elif choice == 2:
    char = str(raw_input("Enter character [ex: A, c]: "))
    print "Character", char, "converts to", ord(char), "in ASCII"
else:
    print "Invalid choice"