ages, age1, age2, age3 = [], [], [], []

print "Enter employee ages (0 = End): "
while True:
    inp = int(raw_input())
    if inp == 0:
        break
    if 26 <= inp <= 35:
        age1.append(inp)
    elif 36 <= inp <= 45:
        age2.append(inp)
    elif 46 <= inp <= 55:
        age3.append(inp)
print "\nAge groups:"
print "26 - 35:", len(age1)
print "36 - 45:", len(age2)
print "46 - 55:", len(age3)