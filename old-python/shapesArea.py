print "Choose shape:"
print "1 = Square / Rectangle"
print "2 = Circle"
choice = int(raw_input("Enter 1 or 2: "))

if choice == 1:
    print "If square, enter side length twice."
    side1 = float(raw_input("Enter length of 1st side: "))
    side2 = float(raw_input("Enter length of 2nd side: "))
    print "Area is", side1*side2
elif choice == 2:
    r = float(raw_input("Enter radius: "))
    print "Area is", 3.14159*r*r