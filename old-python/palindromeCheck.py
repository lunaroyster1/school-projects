string = str(raw_input("Enter string: "))
string = filter(str.isalnum, string).lower()

for x in xrange(len(string)):
    if string[x] != string[len(string)-x-1]:
        print "String is not palindrome"
        break
else:
    print "String is palindrome"