class Number(object):
    def __init__(self, x):
        self.x = x
    def __add__(self, y):
        return self.x + y
    def __sub__(self, y):
        return self.x - y
    def __mul__(self, y):
        return self.x * y
    def __div__(self, y):
        return self.x / y

a = Number(5)
print "a + 5 =", a+5
print "a - 5 =", a-5
print "a * 5 =", a*5
print "a / 5 =", a/5